package com.tccdemo;

import org.springframework.stereotype.Repository;

public class Account
{
    private Integer id;
    private int     amount;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public int getAmount()
    {
        return amount;
    }

    public void setAmount(int amount)
    {
        this.amount = amount;
    }

    @Override
    public String toString()
    {
        return "Account{" + "id=" + id + ", amount=" + amount + '}';
    }
}
