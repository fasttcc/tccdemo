package com.tccdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CancelTransfer implements Transfer
{
    @Autowired
    private AmountOp op;

    @Override
    @Transactional
    public void transfer()
    {
        op.add();
    }
}
