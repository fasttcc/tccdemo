package com.tccdemo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("provider")
public interface ProviderFeignClient
{
    @GetMapping("/add")
    String add();

    @GetMapping("/user")
    User user();
}
