create schema if not exists inst01 collate utf8_general_ci;

create table if not exists account
(
	id int auto_increment
		primary key,
	amount int null,
	froze int null
);

create table if not exists jfiretcc
(
	global_id varchar(100) not null,
	branch_id varchar(100) not null,
	create_time bigint not null,
	identifier varchar(50) null
);

create index jfiretcc_global_id_idx
	on jfiretcc (global_id);

