package com.tccdemo;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class CancelTransfer implements Transfer
{
    @Resource
    private AmountOp op;

    @Override
    @Transactional
    public void transfer()
    {
        op.add();
    }
}
