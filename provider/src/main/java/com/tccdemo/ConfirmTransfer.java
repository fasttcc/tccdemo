package com.tccdemo;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ConfirmTransfer implements Transfer
{
    @Resource
    private AmountOp amountOp;

    @Override
    public void transfer()
    {
        amountOp.confirm();
    }
}
