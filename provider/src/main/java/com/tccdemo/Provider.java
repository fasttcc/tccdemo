package com.tccdemo;

import com.bosssoft.platform.fasttcc.support.spring.EnableTcc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Hello world!
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@RestController
@EnableEurekaClient
@ComponentScan
@RequestMapping(produces = "application/json;charset=UTF-8")
@EnableTcc
public class Provider
{

    @Resource
    private TryTransfer transfer;
    @Resource
    private AmountOp    op;

    @GetMapping("/user")
    @ResponseBody
    public User user()
    {
        User user = new User();
        user.setName("linbin");
        user.setAge(29);
        return user;
    }

    @GetMapping("/account")
    public Account account()
    {
        Account account = op.findById();
        return account;
    }

    @GetMapping("/add")
    public String add()
    {
        transfer.transfer();
        return "success";
    }

    public static void main(String[] args)
    {
        SpringApplication.run(Provider.class, args);
    }
}
