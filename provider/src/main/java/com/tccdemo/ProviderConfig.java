package com.tccdemo;

import com.alibaba.druid.pool.DruidDataSource;
import com.mysql.jdbc.Driver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class ProviderConfig
{
    @Bean("realDataSource")
    public DataSource dataSource()
    {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        dataSource.setUrl("jdbc:mysql://localhost:3306/inst02?characterEncoding=utf8&useSSL=false");
        dataSource.setDriverClassName(Driver.class.getName());
        return dataSource;
    }
}
