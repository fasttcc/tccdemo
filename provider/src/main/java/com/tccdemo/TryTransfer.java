package com.tccdemo;

import com.bosssoft.platform.fasttcc.Tcc;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class TryTransfer implements Transfer
{
    @Resource
    private AmountOp op;

    @Override
    @Transactional
    @Tcc(tccInterface = Transfer.class, cancelClass = CancelTransfer.class, confirmClass = ConfirmTransfer.class)
    public void transfer()
    {
        op.add();
    }
}
